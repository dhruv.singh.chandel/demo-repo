# main.py

def add_numbers(a, b):
    return a + b

def subtract_numbers(a, b):
    return a - b

def multiply_numbers(a, b):
    return a * b

def divide_numbers(a, b):
    if b != 0:
        return a / b
    else:
        return "Cannot divide by zero"

if __name__ == "__main__":
    num1 = 10
    num2 = 5

    # Perform operations
    sum_result = add_numbers(num1, num2)
    diff_result = subtract_numbers(num1, num2)
    product_result = multiply_numbers(num1, num2)
    division_result = divide_numbers(num1, num2)

    # Print results
    print(f"Sum: {sum_result}")
    print(f"Difference: {diff_result}")
    print(f"Product: {product_result}")
    print(f"Division: {division_result}")
